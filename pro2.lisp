;;Solución problema 2 Euler Project
;;

(defun fibonacci (n) 
  (labels ((fibonaccitail (fn1 fn2 n)
           (if (= 1 n)
             fn1
             (fibonaccitail (+ fn1 fn2) fn1 (1- n)))))
    (fibonaccitail 1 0 n)))



