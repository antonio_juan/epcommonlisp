;;recusive version
(defun pro1 (num)
  (labels ((rec (n sum)
             (if (>= n num)
               sum
               (if (or (zerop (mod n 3))
                       (zerop (mod n 5)))
                 (rec (1+ n) (+ sum n))
                 (rec (1+ n) sum)))))
    (rec 1 0)))
